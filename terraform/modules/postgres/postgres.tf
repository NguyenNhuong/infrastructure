# helm install my-release oci://registry-1.docker.io/bitnamicharts/<chart>
# to clean up https://stackoverflow.com/questions/63917524/helm-postgres-password-authentication-failed
#   run: kubectl delete pvc data-postgres-postgresql-0
resource "helm_release" "postgres" {
  name       = "postgres"
  namespace  = "default"
  repository = "oci://registry-1.docker.io/bitnamicharts"
  chart      = "postgresql"

  set {
    name  = "auth.enablePostgresUser"
    value = "true"
  }
  set {
    name  = "auth.postgresPassword"
    value = var.db-admin-password
  }
  set {
    name  = "auth.username"
    value = var.db-username
  }
  set {
    name  = "auth.password"
    value = var.db-password
  }
  set {
    name  = "auth.database"
    value = var.db-name
  }

  # https://stackoverflow.com/questions/57174756/how-do-initialize-a-postgresql-db-in-terraforms-helm-provider
  set {
    name  = "primary.initdb.scripts.init\\.sql"
    value = <<EOF
      CREATE DATABASE keycloak WITH OWNER ${var.db-username};
      CREATE DATABASE kong WITH OWNER ${var.db-username};
      CREATE DATABASE product_db WITH OWNER ${var.db-username};
      CREATE DATABASE limit_db WITH OWNER ${var.db-username};
      CREATE DATABASE internal_transfer_db WITH OWNER ${var.db-username};
      CREATE DATABASE external_transfer_db WITH OWNER ${var.db-username};
      CREATE DATABASE transaction_history_db WITH OWNER ${var.db-username};
    EOF
  }

  set {
    name  = "primary.extendedConfiguration"
    value = <<EOF
      wal_level=logical
      max_wal_senders=1
      max_replication_slots=1
    EOF
  }
}
