resource "helm_release" "apisix" {
  # count            = var.apisix_ingress_controller_enabled ? 1 : 0
  name             = "apisix"
  repository       = "https://charts.apiseven.com/"
  chart            = "apisix"
  namespace        = local.apisix_namespace
  create_namespace = true

  set {
    name  = "gateway.type"
    value = "NodePort"
  }
  set {
    name  = "ingress-controller.enabled"
    value = "true"
  }
  set {
    name  = "ingress-controller.config.ingressPublishService"
    value = "ingress-apisix/apisix-gateway"
  }
  set {
    name  = "ingress-controller.config.apisix.serviceNamespace"
    value = local.apisix_namespace
  }
  set {
    name  = "ingress-controller.config.apisix.adminAPIVersion"
    value = "v3"
  }
  set {
    name  = "apisix.hostNetwork"
    value = "true"
  }
  set {
    name  = "dashboard.enabled"
    value = "true"
  }
}

locals {
  apisix_namespace = "ingress-apisix"
}

resource "kubernetes_ingress_v1" "apisix_admin_ingress" {
  depends_on = [helm_release.apisix]
  metadata {
    name      = "apisix-admin-ingress"
    namespace = local.apisix_namespace
  }
  spec {
    ingress_class_name = "apisix"
    rule {
      host = "admin.apisix.local"
      http {
        path {
          path      = "/"
          path_type = "Prefix"
          backend {
            service {
              name = "apisix-admin"
              port {
                number = 9180
              }
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_ingress_v1" "apisix_dashboard_ingress" {
  depends_on = [helm_release.apisix]

  metadata {
    name      = "apisix-dashboard-ingress"
    namespace = local.apisix_namespace
  }
  spec {
    ingress_class_name = "apisix"
    rule {
      host = "dashboard.apisix.local"
      http {
        path {
          path      = "/"
          path_type = "Prefix"
          backend {
            service {
              name = "apisix-dashboard"
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}


resource "kubernetes_ingress_v1" "apisix_api_ingress" {
  depends_on = [helm_release.apisix, helm_release.nginx-ingress-controller]
  metadata {
    name      = "api-ingress"
    namespace = local.apisix_namespace
  }
  spec {
    ingress_class_name = "default"
    rule {
      host = "*.apisix.local"
      http {
        path {
          path      = "/"
          path_type = "Prefix"
          backend {
            service {
              name = "apisix-gateway"
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}
